 
import React, { Component } from 'react';
import {TouchableOpacity,View ,Linking} from 'react-native';
import {TabHeading,ScrollableTab,Tab,Tabs, StyleProvider ,Container,Card,List,ListItem,thumbnail, CardItem,Grid,Col, Row, Thumbnail , Header, Title,Text, Content, Footer, FooterTab, Button, Left, Right, Body,Icon  } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import { Thumbnail as ThumbnailVideo} from 'react-native-thumbnail-video';

// load compomemts
import HeaderComponent from './components/HeaderComponent';
import GoComponent from './components/GoComponent';
import FooterComponent from './components/FooterComponent';
// import ImageBackgroundComponent from './components/ImageBackgroundComponent';
import styles from './components/styles';
 
type Props = {};
export default class SingleCategory extends Component<Props> {
 
    constructor(props) {
        super(props);
        this.state = { data: [] };
    }
    
    
      componentDidMount() {
    
 
                  this.setState({data:this.props.navigation.getParam('data', [])  })
    
   
    }
 
 
 
 
    render() {
    return (
    
      <StyleProvider style={getTheme(platform)}>

      <Container  >
        
      {/* <ImageBackgroundComponent /> */}


<HeaderComponent back RemoveRight  navigation={this.props.navigation}>{this.props.navigation.getParam('title', 'NO-ID')}</HeaderComponent>
<Content style={{paddingRight:15,backgroundColor:'#eeefff'}}>

                 {/* <Text>  { JSON.stringify( this.state.data) }</Text>     */}

  

 <View style={{}}>

       {this.state.data.map((link,key) => {
            return(
            
                
  
       <ListItem     onPress={ ()=>{ Linking.openURL(link.link)}}       thumbnail   style={{  borderColor:'#e7e7e7',borderWidth:1,borderRadius:20,marginVertical:12,backgroundColor:'#fff'}} key ={key}>
       {link.Thumbnail  ? 
       
          <Left style={{borderColor:'transparent'}}>
            <ThumbnailVideo url={link.link} type='medium' imageHeight={100}  imageWidth={110} showPlayIcon={false} />
          </Left>
        : 
       null
       
        }


                <Body style={{borderColor:'transparent'}}>
                    <Text uppercase={false} numberOfLines={1} style={{ color:"#2196f3",fontSize: 19,marginLeft:0,textAlign:'left',fontWeight:'500'}}>{link.title}</Text>
                    <Text numberOfLines={1}>{link.link}</Text>
                </Body>

                <Right style={{borderColor:'transparent'}}>
                   <GoComponent />
                </Right>
        
        </ListItem>
            
            
            
       
            
            
            );
            })}





  
 </View >

        </Content>


          <FooterComponent navigation={this.props.navigation} />

</Container>
</StyleProvider>


    );
  }
 

}

 
 