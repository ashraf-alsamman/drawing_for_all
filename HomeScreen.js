 
import React, { Component } from 'react';
import { View  ,Alert ,ImageBackground} from 'react-native';


 
import {Spinner, StyleProvider ,Container,Card,List,ListItem,Grid, Row,Col, Thumbnail , Header, Title,Text, Content, Footer, FooterTab, Button, Left, Right, Body,Icon  } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import firebase from './components/firebase';

// load compomemts
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
  // import ImageBackgroundComponent from './components/ImageBackgroundComponent';
import CornerComponent from './components/CornerComponent';

import styles from './components/styles';
const remote = 'https://c.pxhere.com/photos/4a/69/drawing_crayon_graffiti_kids_children_parenting_hand_infatuation-1196592.jpg!d';


 
type Props = {};
export default class HomeScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { data: [] };
}

showData() {

  if (this.state.data.length == 0) {
    return <Spinner color="#2196f3" size='large'  ></Spinner>;
} else {
    return (
    
    
    
    this.state.data.map((link,key) => {
      return(
  
        <Button   key = {key}  style={{flexDirection:"column",flex: 1,  justifyContent: 'center', alignItems: 'center',backgroundColor: '#fff', 
              height: 100,width:130,borderWidth:2,borderColor:'#dcdcdc', borderRadius:13 ,marginTop:15}} 
                onPress={() => this.props.navigation.navigate('SingleCategory',{itemId:link.id,title:link.data().title,data:link.data().data})}
              >
  
            <Icon name={link.data().icon.name} type={link.data().icon.type} style={{ fontSize: 45,color:"#2196f3"}}/>
            <Text uppercase={false} style={{ color:"#797979",fontSize: 15,marginLeft:0,textAlign:'left',fontWeight:'500'}}>{link.data().title}</Text>
        
        </Button>
  
  
      );
  })
    
    
  ) ;  
    
   
   
}

 


}
  componentDidMount() {

    firebase.auth().signInAnonymously().catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
 
 
 
   
             //   Alert.alert('onAuthStateChanged',  JSON.stringify('onAuthStateChanged')   ); 
          

  firebase.firestore().collection("categories").get().then((querySnapshot) => {
     querySnapshot.forEach((doc) => {
         var  joined = this.state.data.concat(doc);
         this.setState({ data: joined });
    });
    
}).catch(function(error) {
      Alert.alert('Error getting documents',  JSON.stringify(error)   );  //  this.setState({ data: error });
});


 
 


}

 


  render() {
  
  
  
    return (
    
<StyleProvider style={getTheme(platform)}>
  <Container>
        <ImageBackground 
                  source={ require('./assets/img/555.png') }
                  style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,   opacity: 0.6}}   >    
        </ImageBackground >
    <HeaderComponent   back={false}  RemoveRight  RemoveLeft navigation={this.props.navigation}>Draw For All</HeaderComponent>
    <Content style={{paddingHorizontal:50}}>

 

       

 
 <View style={{paddingHorizontal:50 }}>
{/* // all content */}

 
 {/* <Button  onPress={ ()=>{ Linking.openURL('https://google.com')}}  ><Text> eeeeeeeeeeeee</Text> </Button> */}

{/* <List style={{margin:0,padding:0, alignContent:'flex-start'}}> */}
                 {/* <Text>  { JSON.stringify(this.state) }</Text>   */}

 <Body style={{ flex: 1,  justifyContent: 'center', alignItems: 'center' }}>
  {/* <Text style={{fontSize:19,color:'#000',marginTop:15}}>  select a category</Text>  */}
 </Body>


                 <Body style={{ flex: 1,  justifyContent: 'center', alignItems: 'center',marginBottom:30 ,marginTop:30}}>
 


 
            
          
{this.showData()}



            </Body>

         
             
 

</View>
{/* // all content */}
        </Content>

         <FooterComponent navigation={this.props.navigation} />
 
</Container>
</StyleProvider>


    );
  }

}

 

 