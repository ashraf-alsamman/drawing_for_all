 
import React, { Component } from 'react';
import { View ,Image,ImageBackground} from 'react-native';
import { Spinner } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
// load compomemts

import ImageBackgroundComponent from './components/ImageBackgroundComponent';
import HeaderComponent from './components/HeaderComponent';
import ButtonComponent from './components/ButtonComponent';
import styles from './components/styles';

  const logo =  require('./assets/img/logo.png') ;


type Props = {};
export default class Splash extends Component<Props> {
 
  constructor(props) {
    super();

}

 
componentDidMount () {
let self = this;
    setTimeout(function () {
      self.props.navigation.navigate('HomeScreen' );

 }, 4000)
 
}
  render() {
    return (
    
 

  <View style={{justifyContent: 'center', alignItems: 'center'  ,flexDirection: 'column',justifyContent: 'center'
,alignItems: 'center',height: '100%',backgroundColor:'#2196f3'}}>
  
  <ImageBackground 
                  source={ require('./assets/img/kids_figure_paint-687194.png') }
                  style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,   opacity: 0.7}}   >    
        </ImageBackground >
  
  <Image
                      style={{width: 299, height: 163,  justifyContent: 'center'}}
                      source={  require('./assets/img/logo.png')  }
                    />
 
 <Spinner color="#fff" size='large' />

</View> 
         
   


    );
  }


}

 
 