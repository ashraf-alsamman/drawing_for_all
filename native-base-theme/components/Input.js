import variable from './../variables/platform';

export default (variables = variable) => {
	const inputTheme = {
		'.multiline': {
			height: null,
		},
		height: variables.inputHeightBase,
		color: variables.inputColor,
		paddingLeft: 0,
		paddingRight: 0,
		flex: 1,
		fontSize: variables.inputFontSize,
        color:'#252525'
	};

	return inputTheme;
};
