import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';
 
// load compomemts
  
 import SingleCategory from './SingleCategory'; 
 import About from './About'; 
 import Splash from './Splash'; 
 import HomeScreen from './HomeScreen'; 
 import Draw from './Draw'; 
 
 
const RootNavigator = createStackNavigator({

  Home: {
    screen: Splash,
  },
  HomeScreen: {
    screen: HomeScreen,
  },  
  SingleCategory: {
    screen: SingleCategory,
  },  
  About: {
    screen: About,
  }, 
  Draw: {
    screen: Draw,
  }, 
  
}, {
    // no header
    headerMode: 'none',
});


 export default   RootNavigator ;