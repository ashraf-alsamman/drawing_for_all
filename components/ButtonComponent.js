 
import React, { Component } from 'react';
import {View,StyleSheet} from 'react-native';
import {Button,Icon,Text} from 'native-base';
import ContainerComponent from './ContainerComponent';

type Props = {};
export default class ButtonComponent extends Component<Props> {
    constructor(props) {
        super(props);


    
    }


  render() {
    RenderButton =  <Button rounded   style={{ flex:1,backgroundColor:'#343434',marginTop:12,paddingVertical:28}}   onPress={this.props.onPress} >
    <Text uppercase={false} style={{fontSize:16,fontFamily:'CircularStd-Medium'}}>{this.props.children}</Text>
 </Button>;

if (this.props.white) {
RenderButton = <Button rounded last style={{flex:1 ,backgroundColor:'#fff',borderColor:'#313131',borderWidth:1,marginTop:12,paddingVertical:28}}   onPress={this.props.onPress} >
   <Text uppercase={false} style={{ color:'#343434',fontSize:16,fontWeight:'400',fontFamily:'CircularStd-Medium'}}>{this.props.children}</Text>
</Button>;
}

if (this.props.outline) {
RenderButton = <Button rounded last style={{flex:1 ,backgroundColor:'transparent',borderColor:'#fff',borderWidth:1,marginTop:12,paddingVertical:28}}   onPress={this.props.onPress} >
   <Text uppercase={false} style={{ color:'#fff',fontSize:16,fontWeight:'400',fontFamily:'CircularStd-Medium'}}>{this.props.children}</Text>
</Button>;
}

if (this.props.orange) {
RenderButton = <Button transparent  last style={{flex:1 ,marginTop:12,paddingVertical:28,width:'100%',justifyContent:'center'}}  onPress={this.props.onPress} >
   <Text uppercase={false} style={{ color:'#e8a13d',fontSize:16,fontWeight:'400',fontFamily:'CircularStd-Medium'}}>{this.props.children}</Text>
</Button>;
}
    return (
         <ContainerComponent>{RenderButton}</ContainerComponent>
    );
  }

 
}

 
 