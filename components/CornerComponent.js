 
import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import {Button,Icon,Text,Left,Right} from 'native-base';
import ContainerComponent from './ContainerComponent';

type Props = {};
export default class CornerComponent  extends Component<Props> {
    constructor(props) {
        super(props);

    }



 
    



  render() {


    RenderButton = 
    <Right style={{flexDirection:'row',justifyContent:'flex-end',alignItems:'center',flex: 1}}   >
        <TouchableOpacity style style={{flexDirection:'row',alignItems:'center'}}  onPress={this.props.onPress} >
          <Text uppercase={false}  style={{ color:"#000",fontSize:14}}>{this.props.children}</Text>
          <Icon name='keyboard-arrow-right' color="#000" type="MaterialIcons"   style={{margin:0,padding:0,fontSize: 22,color:"#000",width:15}}/>
        </TouchableOpacity >
    </Right> ;

    if (this.props.right) {
      RenderButton = 
      <Right style={{flexDirection:'row',justifyContent:'flex-end',alignItems:'center',flex: 1}}   >
          <TouchableOpacity style style={{flexDirection:'row',alignItems:'center'}}  onPress={this.props.onPress} >
            <Text uppercase={false}  style={{ color:"#000",fontSize:14}}>{this.props.children}</Text>
            <Icon name='keyboard-arrow-right' color="#000" type="MaterialIcons"   style={{margin:0,padding:0,fontSize: 22,color:"#000",width:15}}/>
          </TouchableOpacity >
      </Right> ;
    }
    
    if (this.props.left) {
        RenderButton = 
        <Left style={{flex:1}}>
          <TouchableOpacity style style={{flexDirection:'row',alignItems:'center'}}  onPress={this.props.onPress} >
               <Text  uppercase={false} note>{this.props.children}</Text>
          </TouchableOpacity >
        </Left>
      }
    

    return (
         <ContainerComponent>{RenderButton}</ContainerComponent>
    );
  }

 
}

 
 