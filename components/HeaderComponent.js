import React, { Component } from 'react';
import {Text,  StyleSheet} from 'react-native';
import { Header, Title, Button, Left, Right, Body,Icon  } from 'native-base';

 
type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
   



}
  render() {
if (this.props.RemoveLeft) {LeftButton =   null ;}  
 
if (this.props.back) {
  LeftButton = <Button iconRight transparent  style={{marginLeft: 10,backgroundColor:'#2196f3' ,borderColor:'#2196f3', shadowColor: '#2196f3',shadowOpacity: 0,}}    onPress={() => this.props.navigation.goBack()}>
                  <Icon name='ios-arrow-back' type="Ionicons"/>
                </Button>;
} else {
  LeftButton = <Button transparent  onPress={() => this.props.navigation.navigate('MyWallet')}>
                   <Icon name='wallet' type="SimpleLineIcons"/>
                </Button>;
}

if (this.props.RemoveNotifications) {
  RightButton =   null;
} else {
      RightButton = <Button iconRight   style={{marginLeft: 10,backgroundColor:'transparent' ,borderColor:'transparent', shadowColor: 'transparent',shadowOpacity: 0,}}  onPress={() => this.props.navigation.navigate('Notifications')}>
                     <Icon name='content-paste' type="MaterialCommunityIcons"/>
                  </Button>
}

if (this.props.cancel) {
  RightButton = <Button iconRight   style={{marginLeft: 10,backgroundColor:'transparent' ,borderColor:'transparent', shadowColor: 'transparent',shadowOpacity: 0,}}  onPress={() => this.props.navigation.navigate('HomeScreen')}>
<Text style={{color:'#e8a13d',fontSize:17}}>Cancel</Text>
</Button>
} 
if (this.props.RemoveRight) {RightButton =   null ;}   
if (this.props.RemoveLeft) {LeftButton =   null ;}   


    return (
 
        <Header style={{backgroundColor: '#2196f3'}}>
           
          <Left style={{flex:1}}>
              {LeftButton}
          </Left>

          <Body style={styles.title}>
            <Title style={{color:'#fff' ,fontSize:18, fontWeight:'500',fontFamily:'CircularStd-Medium'}}>{this.props.children}</Title>
          </Body>
 
          <Right style={{flex:1}}>
             {RightButton}
          </Right>

        </Header>
 
    );
  }
 

}

 
  const styles = StyleSheet.create({
 
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    },
 
    title: {
      flex: 2,  justifyContent: 'center', alignItems: 'center' 
    },
 
    Header: {
      backgroundColor:'transparent',  justifyContent: 'center', alignItems: 'center' 
    },
    backgroundImage:{
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
    } 
  
  });
