/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
 
import {Button,Footer,FooterTab,Icon,Text} from 'native-base';
 
type Props = {};
export default class FooterComponent extends Component<Props> {






 



  render() {
    return (

      // this.props.screenProps.rootNavigation.navigate('CategoriesScreen') 

      <Footer >
      <FooterTab  style={styles.Footer}>
        <Button vertical   onPress={() => this.props.navigation.navigate('HomeScreen')}>
          <Icon name="home-outline" style={styles.FooterBottunIcon}  type="MaterialCommunityIcons" />
          <Text style={styles.FooterBottunText} >Home</Text>
        </Button>
        {/* <Button vertical  onPress={() => this.props.navigation.navigate('MyWallet')}>
          <Icon name="wallet"  style={styles.FooterBottunIcon}   type="SimpleLineIcons"/>
          <Text style={styles.FooterBottunText} >Wallet</Text>
        </Button>
        <Button vertical active>
          <Icon active name="qrcode"  type="FontAwesome" style={styles.FooterBottunIcon} />
          <Text style={styles.FooterBottunText}>Qr Code</Text>
        </Button>
        <Button vertical  onPress={() => this.props.navigation.navigate('Categories')}>
          <Icon name="shop"  style={styles.FooterBottunIcon}  type="Entypo"/>
          <Text style={styles.FooterBottunText} >Store</Text>
        </Button> */}
        <Button vertical  onPress={() => this.props.navigation.navigate('Draw')}>
          <Icon  style={styles.FooterBottunIcon} name="paint-brush" type="FontAwesome" />
          <Text style={styles.FooterBottunText} >Draw</Text>
        </Button>
        <Button vertical  onPress={() => this.props.navigation.navigate('About')}>
          <Icon  style={styles.FooterBottunIcon} name="more-horiz" type="MaterialIcons" />
          <Text style={styles.FooterBottunText} >About</Text>
        </Button>
      </FooterTab>
    </Footer>


    );
  }

 
}

 
  const styles = StyleSheet.create({
    FooterBottunIcon: {
      color:'#9DABB2',
      fontSize: 19,
    },
    FooterBottunText: {
      color:'#9DABB2',
      fontSize: 9,
    },
 
 
  });
