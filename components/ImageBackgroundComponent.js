 
import React, { Component } from 'react';
import {  StyleSheet, ImageBackground} from 'react-native';
 

const remote = 'https://c.pxhere.com/photos/4a/69/drawing_crayon_graffiti_kids_children_parenting_hand_infatuation-1196592.jpg!d';


type Props = {};
export default class ImageBackgroundComponent extends Component<Props> {
  render() {
    return (
 
         <ImageBackground 
                  source={{uri: remote}}
                  style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,   opacity: 0.6}}   >    
        </ImageBackground >
         
 
    );
  }
 

}

 
  const styles = StyleSheet.create({
 
 
    backgroundImage:{
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
    } 
  
  });
