 
import * as firebase from 'firebase'
require("firebase/firestore");
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');
 
  let config = {
    apiKey: "AIzaSyCjDpjI_k5g4h0mz81h6dx4DieIqbVLJmA",
    authDomain: "drawing-for-all.firebaseapp.com",
    databaseURL: "https://drawing-for-all.firebaseio.com",
    projectId: "drawing-for-all",
    storageBucket: "drawing-for-all.appspot.com",
    messagingSenderId: "332700596493",

    debug: true
  }


   firebase.initializeApp(config);


const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

export default firebase ;